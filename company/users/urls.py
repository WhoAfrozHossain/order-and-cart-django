from django.urls import path
from django.contrib.auth import views as auth_views

from users import views as user_views


urlpatterns = [
    path('', user_views.home, name='home'),
    path('signup', user_views.register, name='signup'),
    path('login', user_views.login, name='login'),
    path('logout', auth_views.LogoutView.as_view(template_name='logout.html'), name='logout'),
]