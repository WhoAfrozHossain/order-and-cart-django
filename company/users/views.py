from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.contrib import messages
from .forms import UserRegistrationForm, UserLoginForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as auth_login

def home(request):
    if(request.user.is_authenticated):
        context = {'name': request.user.first_name+" "+request.user.last_name}
        return render(request, 'home.html', context)
    else:
        return redirect('login')

def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            form.save()

            messages.success(request, f'Your account has been created. You can log in now!')    
            return redirect('login')
    else:
        form = UserRegistrationForm()

    context = {'form': form}
    return render(request, 'register.html', context)

def login(request):
    if request.method == 'POST':
        form = AuthenticationForm(request, data=request.POST)
        print(form.data)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            
            if user is not None:
                auth_login(request, user)
                messages.success(request, f'Successfully logged in')    
                return redirect('/')
            else:
                messages.error(request,"Invalid username or password.")
        
    else:
        form = AuthenticationForm()

    context = {'form': form}
    return render(request, 'login.html', context)
