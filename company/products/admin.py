from django.contrib import admin
from .models import Category, Product, Order, OrderDetails, Customer


class CategoryAdmin(admin.ModelAdmin):
  list_display = ("name", "created_at", "updated_at",)
  readonly_fields = ("created_at", "updated_at")

class ProductAdmin(admin.ModelAdmin):
  list_display = ("name", "price", "code", 'unit_in_stock')
  readonly_fields = ("created_at", "updated_at")

class OrderAdmin(admin.ModelAdmin):
  list_display = ("customer", "shipping_address", "order_amount", 'shipping_charge', 'discount', 'total_amount')
  readonly_fields = ("created_at", "updated_at")

class OrderDetailsAdmin(admin.ModelAdmin):
  list_display = ("order", "product", "unit_price", 'quantity')
  readonly_fields = ("created_at", "updated_at")

class CustomerAdmin(admin.ModelAdmin):
  list_display = ("name", "email", "phone", 'point', 'member_label')
  readonly_fields = ("created_at", "updated_at")


admin.site.register(Category, CategoryAdmin)  
admin.site.register(Product, ProductAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(OrderDetails, OrderDetailsAdmin)
admin.site.register(Customer, CustomerAdmin)