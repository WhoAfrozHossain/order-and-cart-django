from django.shortcuts import render, redirect, HttpResponse
from products.forms import ProductForm
from products.models import Product, Category, Customer, Order, OrderDetails
from django.db.models.functions import Lower
from django.db.models import Q, F
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from cart.cart import Cart
from django.http.response import JsonResponse
import json
import xlwt
from datetime import datetime
from .export_pdf import html_to_pdf
from django.template.loader import render_to_string
from django.views.generic import View
from django.core.mail import EmailMessage
import requests
import pandas as pd
from django.core.files.storage import FileSystemStorage


def allProducts(request):
  
    searchText = request.GET.get('search')
    sortText = request.GET.get('sort')

    if(searchText):
        products = Product.objects.annotate(
            name_lower=Lower('name'),
            cat_name_lower=Lower('category__name')
        ).filter(
            Q(name_lower__icontains=searchText.lower())
            | Q(cat_name_lower__icontains=searchText.lower())
            | Q(code__icontains=searchText)
        )
    elif(sortText):
        products = Product.objects.all().order_by(sortText)
    else:
        products = Product.objects.all()

    if(searchText == None):
        searchText = ''
    if(sortText == None):
        sortText = ''

    page = request.GET.get('page', 1)

    paginator = Paginator(products, 5)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

    cartSession = request.session.get('cart')
    cartItems = list(cartSession.values())
    cartIds = []
    for c in cartItems :
        cartIds.append(c['product_id'])

    return render(request,"product_list.html", {'products':data, 'carts':cartIds, 'search':searchText, 'sort':sortText})


# def createProduct(request):
#     category = Category.objects.all()

#     if request.method == "POST":
#         form = ProductForm(request.POST, request.FILES)
#         if form.is_valid():
#             try:
#                 form.save()
#                 return redirect('/')
#             except:
#                 pass
#     else:
#         form = ProductForm()
    
#     return render(request,'product_create.html', {'form':form, 'category': category})


def viewProduct(request, id):
    product = Product.objects.get(id=id)
    return render(request,'product_view.html', {'product':product})


# def updateProduct(request, id):
#     product = Product.objects.get(id=id)
#     category = Category.objects.all()

#     if request.method == "POST":
#         form = ProductForm(request.POST, request.FILES, instance = product)
#         if form.is_valid():
#             try:
#                 form.save()
#                 return redirect("/")
#             except:
#                 pass

#     return render(request, 'product_edit.html', {'product':product, 'category': category})


# def addProductStock(request):
#     productId = request.POST.get('product_id')
#     stock=request.POST.get('new_stock')

#     Product.objects.filter(id=productId).update(stock=F('stock')+int(stock))
#     return redirect("/product")


# def deleteProduct(request, id):
#     product = Product.objects.get(id=id)
#     product.delete()
#     return redirect("/product")


def checkout(request):
    cartSession = request.session.get('cart')
    cartItems = list(cartSession.values())

    data = []
    orderAmount = 0
    shippingCharge = 0
    discount = 0

    for c in cartItems :
        product = Product.objects.get(id=c['product_id'])
        data.append({'product': product, 'quantity': c['quantity']})

        orderAmount += (product.price * c['quantity'])
        
    if request.method == "POST":
        customer_name = request.POST.get('name')
        customer_email = request.POST.get('email')
        customer_phone = request.POST.get('phone')
        shipping_address = request.POST.get('address')

        currentCustomer = Customer.objects.filter(phone=customer_phone).first()

        if(currentCustomer == None):
            cId = Customer.objects.create(
                name=customer_name,
                email=customer_email,
                phone=customer_phone,
            )
            
            currentCustomer = Customer.objects.filter(phone=customer_phone).first()
        else:
            cId = currentCustomer.id

        orderId = Order.objects.create(
            customer=currentCustomer,
            order_amount=orderAmount,
            shipping_charge=shippingCharge,
            discount=discount,
            total_amount=(orderAmount+shippingCharge)-discount,
            shipping_address=shipping_address,
        )

        for item in data:
            OrderDetails.objects.create(
                order=orderId,
                product=item['product'],
                unit_price=item['product'].price,
                quantity=item['quantity'],
            )
            Product.objects.filter(id=item['product'].id).update(unit_in_stock=F('unit_in_stock')-int(item['quantity']))

        cart_clear(request)

        point = currentCustomer.point+(orderAmount/100)
        memberLabel = ""
        
        if point > 1000:
            memberLabel = "VIP Member"
        elif point > 500:
            memberLabel = "Gold Member"
        elif point > 0:
            memberLabel = "Silver Member"
        else:
            memberLabel = "Member"

        Customer.objects.filter(id=currentCustomer.id).update(point=point, member_label=memberLabel)

        return redirect("/")
    else:
        return render(request, 'checkout.html', {'products':data, 'total_price': orderAmount})


def cart_add(request, id):
    cart = Cart(request)
    product = Product.objects.get(id=id)
    
    cartSession = request.session.get('cart')
    cartItems = list(cartSession.values())
    
    foundInCart = False

    for c in cartItems :
        if c['product_id'] == id and (c['quantity']) < product.unit_in_stock:
            foundInCart = True
            cart.add(product=product)

    if(foundInCart == False and product.unit_in_stock > 0):
        cart.add(product=product)


    return redirect("/")

def itemIncrementDecrement(request):
    is_ajax = request.headers.get('X-Requested-With') == 'XMLHttpRequest'

    if is_ajax:
        productId = request.GET.get('product_id')
        status = request.GET.get('status')

        cart = Cart(request)
        product = Product.objects.get(id=productId)
        cartSession = request.session.get('cart')
        cartItems = list(cartSession.values())

        if status == 'plus':
            notFound = True

            for c in cartItems :
                if c['product_id'] == product.id and (c['quantity']) < product.unit_in_stock:
                    notFound = False
                    cart.add(product=product)

            if notFound:
                cart.add(product=product)

        elif status == 'minus':
            found = False

            for c in cartItems :
                if c['product_id'] == product.id and (c['quantity']) > 1:
                    found = True
                    cart.decrement(product=product)

                    
            if found == False:
                item_clear(request, product.id)

        elif status == 'remove':
            item_clear(request, product.id)

        cartSession = request.session.get('cart')
        cartItems = list(cartSession.values())

        orderAmount = 0
        shippingCharge = 0
        discount = 0
        qty = 0

        for c in cartItems :
            product = Product.objects.get(id=c['product_id'])
            orderAmount += (product.price * c['quantity'])

            if int(productId) == int(c['product_id']):
                qty = c['quantity']

        data = {
            'quantity': qty,
            'order_amount': orderAmount,
            'shipping_charge': shippingCharge,
            'discount': discount,
            'total_price': (orderAmount+shippingCharge)-discount,
        }
        dump = json.dumps(data)

        return JsonResponse(dump, safe=False)
    else:
        pass
    
def item_clear(request, id):
    cart = Cart(request)
    product = Product.objects.get(id=id)
    cart.remove(product)
    return redirect("/")

def cart_clear(request):
    cart = Cart(request)
    cart.clear()
    return redirect("/")


def export_products(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="products.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    
    styleTitle = xlwt.easyxf('font: name Calibri, height 360, bold on, colour_index black; align: wrap on, vert center, horiz center; border : bottom thin, right thin, top thin, left thin;')
    styleColumnTitle = xlwt.easyxf('font: name Calibri, height 280, bold on; align: wrap on, vert center, horiz center; ')
    styleName = xlwt.easyxf('font: name Calibri, height 280, color-index green, bold on; align: wrap on, vert center, horiz left; ')
    styleId = xlwt.easyxf('font: name Calibri, height 280; align: wrap on, vert center, horiz center; ')
    styleCategoryName = xlwt.easyxf('font: name Calibri, height 280; align: wrap on, vert center, horiz center; ')
    stylePrice = xlwt.easyxf('font: name Calibri, height 280, bold on; align: wrap on, vert center, horiz right; ', num_format_str='#,##0.00')
    styleStock = xlwt.easyxf('font: name Calibri, height 280; align: wrap on, vert center, horiz center; ')
    styleCode = xlwt.easyxf('font: name Calibri, height 280; align: wrap on, vert center, horiz center; ')
    styleCreated = xlwt.easyxf('font: name Calibri, height 280; align: wrap on, vert center, horiz center; ')
    styleSummary = xlwt.easyxf('font: name Calibri, height 280; align: wrap on, vert center, horiz center; border : bottom thin, right thin, top thin, left thin;')

    ws = wb.add_sheet('Products')
    row_num = 2

    columns = ['ID', 'Name', 'Category', 'Price', 'Stock', 'Code', 'Create Date']

    ws.write_merge(0, 0, 0, 6, 'Product List', styleTitle)
    for col_num in range(len(columns)):
        ws.write(1, col_num, columns[col_num], styleColumnTitle)

    rows = Product.objects.all().values_list('id', 'name', 'category__name', 'price', 'unit_in_stock', 'code', 'created_at')
    for row in rows:
        ws.write(row_num, 0, row[0], styleId)
        ws.write(row_num, 1, row[1], styleName)
        ws.write(row_num, 2, row[2], styleCategoryName)
        ws.write(row_num, 3, row[3], stylePrice)
        ws.write(row_num, 4, row[4], styleStock)
        ws.write(row_num, 5, row[5], styleCode)
        ws.write(row_num, 6, row[6].strftime("%d %b, %Y"), styleCreated)
        row_num += 1
        
    ws.write(row_num+4, 5, "Total Product", styleSummary)
    ws.write(row_num+4, 6, xlwt.Formula("Count(A3:A"+str(row_num)+")"), styleSummary)
    ws.write(row_num+5, 5, "Total Stock", styleSummary)
    ws.write(row_num+5, 6, xlwt.Formula("SUM(E3:E"+str(row_num)+")"), styleSummary)

    ws.col(0).width = int(10*260)
    ws.col(1).width = int(40*260)
    ws.col(2).width = int(30*260)
    ws.col(3).width = int(20*260)
    ws.col(4).width = int(20*260)
    ws.col(5).width = int(20*260)
    ws.col(6).width = int(30*260)

    wb.save(response)
    return response


def send_email_with_attachment(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        email = request.POST.get('email')
        message = request.POST.get('message')

        context = {'name': name, 'email': email, 'message': message}
        email_body = render_to_string('email_template.html', context)

        email_subject = 'New message from {} ({})'.format(name, email)
        sender = 'afrozhossain97@gmail.com'
        recipients = [email]
        email = EmailMessage(email_subject, email_body, sender, recipients)

        email.content_subtype = 'html'

        file = request.FILES.get('attachment')
        if file:
            email.attach(file.name, file.read(), file.content_type)

        email.send()

        return redirect('/')

    return redirect('/')


def api_data(request):
    try:
        response = requests.get('https://jsonplaceholder.typicode.com/posts')
        posts = response.json()
    except:
        posts = []

    page = request.GET.get('page', 1)
    paginator = Paginator(posts, 20)

    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

    return render(request, 'api_view.html', {'posts':data})


def read_excel(request):
    img_file = request.FILES['attachment']
    
    fs = FileSystemStorage()
    filename = fs.save(img_file.name, img_file)
    uploaded_file_path = fs.path(filename)
    # print('absolute file path', uploaded_file_path)    

    df = pd.read_excel(uploaded_file_path)
    data = []

    header = []

    for col in df.columns:
        header.append(col)

    for index, row in df.iterrows():
        data_list = []

        for hed in header :
            data_list.append(row[hed])

        data.append(data_list)

        # print(row)
        # data.append({'id': row['ProductID'], 'name': row['ProductName'], 'price': row['UnitPrice'], 'stock': row['UnitsInStock']})
        # print(data)

    # for index, obj in enumerate(data):
    #     print(obj)
    #     print('/n/n/n/n/n/n/n/n/n/n')

    #     for de in obj[str(index)]:
    #         print(de)
            
    context= {'header': header, 'data': data}

    return render(request, 'excel_read_data.html', context)


class GeneratePdf(View):
    def get(self, request, *args, **kwargs):
        products = Product.objects.all()
        base_url = f"{ request.scheme }://{ request.META.get('HTTP_HOST') }"
        pdf = html_to_pdf('pdf.html', {'products': products, 'base_url': base_url})
        
        return HttpResponse(pdf, content_type='application/pdf')