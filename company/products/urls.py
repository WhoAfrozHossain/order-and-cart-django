from django.urls import path
from . import views
from .views import GeneratePdf

urlpatterns = [
    path('',views.allProducts),
    # path('create', views.createProduct),
    path('view/<int:id>', views.viewProduct),
    # path('update/<int:id>', views.updateProduct),
    # path('addStock/', views.addProductStock),
    # path('delete/<int:id>', views.deleteProduct),
    path('export-in-excel', views.export_products, name="excel-export"),
    path('export-pdf', GeneratePdf.as_view(), name="pdf-export"),
    path('send-attachment-mail', views.send_email_with_attachment, name="send-attachment-mail"),
    path('load-api-data', views.api_data, name="load-api-data"),
    path('read-excel', views.read_excel, name="read-excel"),

    path('checkout', views.checkout),


    path('cart/add/<int:id>/', views.cart_add, name='cart_add'),
    path('cart/item_clear/<int:id>/', views.item_clear, name='item_clear'),
    path('cart/item-increment-decrement',views.itemIncrementDecrement, name='itemIncrementDecrement'),
    path('cart/cart_clear/', views.cart_clear, name='cart_clear'),
]