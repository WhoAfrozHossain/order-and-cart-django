# Generated by Django 4.1.6 on 2023-02-13 12:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0004_remove_product_unit_in_order'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='order_amount',
            field=models.FloatField(default=0),
        ),
        migrations.AddField(
            model_name='order',
            name='total_amount',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='order',
            name='discount',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='order',
            name='shipping_charge',
            field=models.FloatField(default=0),
        ),
    ]
